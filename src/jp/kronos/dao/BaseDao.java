package jp.kronos.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class BaseDao {

	// JDBCドライバ
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

	// URL
	private static final String URL = "jdbc:mysql://localhost:3306/knowledge?characterEncoding=UTF-8&serverTimezone=JST";

	// ユーザ
	private static final String USER = "root";

	// パスワード
	private static final String PASSWORD = "scsk";

	/**
	 * DB接続
	 * @return Connectionオブジェクト
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(DRIVER);
		Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
		return con;
	}
}
