package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kronos.dto.User;

public class UserDao extends BaseDao {

	/**
	 * ユーザ情報を取得する
	 * @param メールアドレス
	 * @param パスワード
	 * @return ユーザ情報
	 */
	public User getUserByIdAndPassword(String email, String pw) {
		User user = null;
		String sql = "SELECT * FROM user WHERE email = ? AND password = SHA2(?, 256)";

		try (Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setString(1, email);
			ps.setString(2, pw);

			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				user = new User();
				user.setUserId(rs.getInt("user_id"));
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setAdministratorFlg(rs.getInt("administrator_flg"));
				user.setUpdateUserId(rs.getInt("update_user_id"));
				user.setUpdateAt(rs.getTimestamp("update_at"));
				user.setDelFlg(rs.getInt("del_flg"));
			}

		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	// 全データの取得
	public List<User> selectUser() throws Exception {
		List<User> list = new ArrayList<User>();
		String sql = "SELECT * FROM user WHERE del_flg = 0";

		try (Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("user_id"));
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setAdministratorFlg(rs.getInt("administrator_flg"));
				user.setUpdateUserId(rs.getInt("update_user_id"));
				user.setUpdateAt(rs.getTimestamp("update_at"));
				user.setDelFlg(rs.getInt("del_flg"));
				list.add(user);
				}
			} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}

		return list;
	}

	public int deleteUser(int id) throws Exception {
		String sql = "UPDATE user SET del_flg = 1 WHERE user_id = ? ";
		try(Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)){
			ps.setInt(1,id);
			if(ps.executeUpdate() == 1) {
				return 1;
				}
		} catch (ClassNotFoundException | SQLException e) {
			throw e;
			}
		return 0;
	}

	public User getById(int userId) throws Exception {
		User user = null;
		String sql = "SELECT * FROM user WHERE user_id = ? ";

		try (Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				user = new User();
				user.setUserId(rs.getInt("user_id"));
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setAdministratorFlg(rs.getInt("administrator_flg"));
				user.setUpdateUserId(rs.getInt("update_user_id"));
				user.setUpdateAt(rs.getTimestamp("update_at"));
				user.setDelFlg(rs.getInt("del_flg"));

			}

		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}
		return user;
	}

}
