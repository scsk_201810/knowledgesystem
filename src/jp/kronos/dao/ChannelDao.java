package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kronos.dto.Channel;

public class ChannelDao extends BaseDao {

	/**
	 * チャンネルの全データ取得
	 * @return 全チャンネルデータ
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<Channel> selectChannel() throws ClassNotFoundException,SQLException{
		List<Channel> list = new ArrayList<Channel>();
		String sql = "SELECT * FROM channel ORDER BY update_at desc";

		try (Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				Channel channel = new Channel();
				channel.setChannelId(rs.getInt("channel_id"));
				channel.setChannelName(rs.getString("channel_name"));
				channel.setOverview(rs.getString("overview"));
				channel.setUserId(rs.getInt("user_id"));
				channel.setUpdateAt(rs.getTimestamp("update_at"));
				list.add(channel);
			}
		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}

		return list;
	}

	/**
	 * チャンネルの全データ取得
	 * @param チャンネルID
	 * @return 処理行数
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int deleteChannel(int channelId) throws ClassNotFoundException,SQLException {
		String sql = "DELETE FROM channel WHERE channel_id = ?";
		int line = 0;
		try (Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement(sql)) {

			ps.setInt(1, channelId);
			line = ps.executeUpdate();

		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}

		return line;
	}

	/**
	 * 引数のチャンネルIDを元にデータを取得する
	 * @param チャンネルID
	 * @return 取得したチャンネルデータ
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Channel getChannelById(int id) throws ClassNotFoundException, SQLException {
		Channel ch = null;
		String sql = "SELECT * FROM channel WHERE channel_id = ?";

		try (Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				ch = new Channel();
				ch.setChannelId(rs.getInt("channel_id"));
				ch.setChannelName(rs.getString("channel_name"));
				ch.setOverview(rs.getString("overview"));
				ch.setUserId(rs.getInt("user_id"));
				ch.setUpdateAt(rs.getTimestamp("update_at"));
			}

		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}

		return ch;
	}
}