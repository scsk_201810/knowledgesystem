package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.kronos.dto.Knowledge;

public class KnowledgeDao extends BaseDao {

	public Knowledge getKnowledge(int knowledgeID) throws Exception {
		Knowledge knowledge = null;
		String sql = "SELECT * FROM knowledge WHERE knowledge_id = ?" ;

		try (Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement(sql)) {
			 	ps.setInt(1,knowledgeID );

				ResultSet rs = ps.executeQuery();

				if(rs.next()) {
					knowledge = new Knowledge();
					knowledge.setKnowledgeId(rs.getInt("knowledge_id"));
					knowledge.setKnowledge(rs.getString("knowledge"));
					knowledge.setChannelId(rs.getInt("channel_id"));
					knowledge.setUserId(rs.getInt("user_id"));
					knowledge.setUpdateAt(rs.getTimestamp("update_at"));
				}

		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}
		return knowledge;
	}

	public void updateKnowledge(int knowledgeID,String knowledge,int channelID) throws Exception {

		String sql = "UPDATE knowledge SET knowledge = ?,channel_id = ? WHERE knowledge_id = ?";//仮に4を設定。本当は一覧画面から受け取ったナレッジID
		try (Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement(sql)) {
			 	ps.setString(1,knowledge );
			 	ps.setInt(2,channelID );
			 	ps.setInt(3,knowledgeID );

				ps.executeUpdate();
				} catch(ClassNotFoundException | SQLException e) {
					throw e;
		}
	}

	/**
	 * 指定したチャンネルの全ナレッジを取得する
	 * @param チャンネルID
	 * @return ナレッジ一覧
	 */
	public List<Knowledge> getKnowledgeByChannelId(int cid) throws ClassNotFoundException, SQLException {
		List<Knowledge> list = new ArrayList<>();
		String sql = "SELECT k.knowledge_id, k.knowledge, k.channel_id, k.user_id, CONCAT(u.last_name, ' ', u.first_name) user_name, k.update_at "
					+ "FROM knowledge k INNER JOIN user u ON k.user_id = u.user_id "
					+ "WHERE k.channel_id = ? "
					+ "ORDER BY k.update_at DESC, k.knowledge_id DESC";

		try(Connection conn = getConnection();
			PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setInt(1, cid);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				Knowledge k = new Knowledge();
				k.setKnowledgeId(rs.getInt("knowledge_id"));
				k.setKnowledge(rs.getString("knowledge"));
				k.setChannelId(rs.getInt("channel_id"));
				k.setUserId(rs.getInt("user_id"));
				k.setUpdateAt(rs.getTimestamp("update_at"));
				k.setUserName(rs.getString("user_name"));
				list.add(k);
			}

		} catch(ClassNotFoundException | SQLException e) {
			throw e;
		}

		return list;
	}

	/**
	 * ナレッジの追加
	 * @param ナレッジ
	 */
	public void insertKnowledge(Knowledge knowledge) throws ClassNotFoundException, SQLException {
		String sql = "insert into KNOWLEDGE(KNOWLEDGE, CHANNEL_ID, USER_ID) values (?, ?, ?)";
		try (Connection conn = getConnection();
				PreparedStatement ps = conn.prepareStatement(sql)) {

				ps.setString(1, knowledge.getKnowledge());
				ps.setInt(2, knowledge.getChannelId());
				ps.setInt(3, knowledge.getUserId());

				ps.executeUpdate();

			} catch(ClassNotFoundException | SQLException e) {
				throw e;
			}
	}
}

