package jp.kronos.test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.KnowledgeDao;

class UpdateKnowledgeTest {

	@Test
	void test() throws Exception {
		KnowledgeDao knowledgedao = new KnowledgeDao();
		knowledgedao.updateKnowledge(1,"あああ",2);
		assertThat(knowledgedao.getKnowledge(1).getKnowledge(),is("あああ"));

	}

}
