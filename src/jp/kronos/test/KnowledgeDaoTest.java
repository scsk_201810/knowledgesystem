package jp.kronos.test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.Knowledge;

class KnowledgeDaoTest {

	@Test
	void insertKnowledgeTest() {
		Knowledge knowledge = new Knowledge();
		KnowledgeDao knowledgeDao = new KnowledgeDao();
		knowledge.setChannelId(1);
		knowledge.setKnowledge("test");
		knowledge.setUserId(1);
		try {
			knowledgeDao.insertKnowledge(knowledge);
			System.out.println("要データベース確認");
		} catch (SQLException|ClassNotFoundException exception) {
			fail(exception.getMessage());
			exception.printStackTrace();
		}
	}

	@Test
	void getKnowledgeByChannelIdTest() {

		KnowledgeDao knowledgeDao = new KnowledgeDao();
		try {
			List<Knowledge> knowledgeList = knowledgeDao.getKnowledgeByChannelId(1);
			Object[][] list=
				   {{"Javaを習得できれば、Webサービスだけではなく組込み系やデスクトップアプリ等、大小様々なシステムで活用できます。OSに依存せず、ライブラリも豊富なので開発の幅が広く、有名なサービスではTwitterやEvenoteでもJavaが使用されています。また、最近はJavaを使った機械学習も行われています。", 1, 1},
				    {"Java は 「プラットフォームに依存しないプログラミング言語である」など多くの有益な面を持っており、今日のWebアプリケーション開発において重要なポジションを占めています。", 1, 3},
					{"Javaは言語として高性能で、OSに依存せずに動作すること、開発効率と保守性が高いこと、ライブラリが充実していることなどのメリットがあります。Javaは活用範囲が広く、昔から根強い人気があるプログラミング言語です。", 1, 1}};

			int i=0;
			for(Knowledge knowledge : knowledgeList) {
				System.out.println(knowledge.getKnowledgeId());
				System.out.println(knowledge.getKnowledge());
				System.out.println(knowledge.getChannelId());
				System.out.println(knowledge.getUserId());
				System.out.println(knowledge.getUpdateAt());
			}
			i=0;
			for(Knowledge knowledge : knowledgeList) {
				assertThat(knowledge.getKnowledge(), is((String)list[i][0]));
				assertThat(knowledge.getChannelId(), is((int)list[i][1]));
				assertThat(knowledge.getUserId(), is((int)list[i][2]));
				i++;
			}
		} catch (SQLException|ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック

		}
	}

}
