package jp.kronos.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.Knowledge;

class InsertKnowledgeTest {

	@Test
	void test() {
		Knowledge knowledge = new Knowledge();
		KnowledgeDao knowledgeDao = new KnowledgeDao();
		knowledge.setChannelId(1);
		knowledge.setKnowledge("test");
		knowledge.setUserId(1);
		try {
			knowledgeDao.insertKnowledge(knowledge);
			System.out.println("要データベース確認");
		} catch (SQLException|ClassNotFoundException exception) {
			fail(exception.getMessage());
			exception.printStackTrace();
		}
	}

}
