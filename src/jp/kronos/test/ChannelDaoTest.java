package jp.kronos.test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.Channel;

class ChannelDaoTest {

	@Test
	void getChannel() {

		ChannelDao channelDao = new ChannelDao();
		try {
		List<Channel> channelList = channelDao.selectChannel();
		Object[][] list =
			   {{1,"Java基礎", "Pure Java関連", 1},
				{2,"SpringFramework", "SpringFrameworkについてのナレッジを共有します。", 1},
				{3,"Jenkins", "Jenkinsについてのナレッジを共有します。", 3},
				{4,"Git", "構成管理ツールのデファクトスタンダードになりつつあるGitを学ぼう", 2},
				{5,"AWS", "AWSについてのナレッジを共有します。", 2},
				{6,"Linux", "Linuxについてのナレッジを共有します。", 3},
				{7,"AI・機械学習", "AI・機械学習についてのナレッジを共有します。", 1},
				{8,"Python", "Pythonについてのナレッジを共有します。", 3},
				{9,"PHP Cake", "PHP Cakeについてのナレッジを共有します。", 3},
				{10,"セキュリティ", "セキュリティについてのナレッジを共有します。", 1},
				{11,"Ruby", "Rubyについてのナレッジを共有します。", 4}};

		for(Channel channel : channelList){
			System.out.println(channel.getChannelId());
			System.out.println(channel.getChannelName());
			System.out.println(channel.getOverview());
			System.out.println(channel.getUserId());
			System.out.println(channel.getUpdateAt());
		}
		int i=0;
		for(Channel channel : channelList){
			assertThat(channel.getChannelId(), is((int)list[i][0]));
			assertThat(channel.getChannelName(), is((String)list[i][1]));
			assertThat(channel.getOverview(), is((String)list[i][2]));
			assertThat(channel.getUserId(), is((int)list[i][3]));
			i++;
		}
		}catch(ClassNotFoundException|SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	void deleteChannel() {

		ChannelDao channelDao = new ChannelDao();
		try {
			channelDao.deleteChannel(9);
		}catch(ClassNotFoundException|SQLException e) {
			e.printStackTrace();
		}
	}
}
