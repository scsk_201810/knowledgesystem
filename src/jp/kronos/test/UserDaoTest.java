package jp.kronos.test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.UserDao;
import jp.kronos.dto.User;

class UserDaoTest {

	@Test
	void selectTest() {
		UserDao userdao = new UserDao();
		int i = 0;
		try {
			List<User> userList =userdao.selectUser();
			Object[][] list = {{1, "nobunaga_oda@gmail.com","a61dcc4d3422bdbc14c1a69b030ee709713127dd0b8b51491ff7b886641154de", "信長", "織田", 0, 1, "2018-10-22 14:42:37", 0},
                    {2, "hideyoshi_toyotomi@gmail.com", "0370f59b567315ff479aeb220c7623c75a9c6aad5c717e135377262e4d2b408b", "秀吉", "豊臣", 1, 1, "2018-10-22 14:42:37", 0},
                    {4, "hidemitsu_akechi@gmail.com", "8d9c4e4cac9820654dbbd72bdd8c549ea9e72e3b8f61ad7bc533dfe6ebf4f707", "秀光", "明智", 1, 3, "2018-10-22 14:42:37", 0},
                    {14, "mitsunari_ishida@gmail.com", "055350cc75265713838f10195932f8ee56d6ca661bbb89215e547f0aea33dbe3", "光成", "石田", 0, 3, "2018-10-30 15:00:16", 0}};
			for(User user : userList) {
				assertThat(user.getUserId(), is(list[i][0]));
				assertThat(user.getEmail(), is(list[i][1]));
				assertThat(user.getFirstName(), is(list[i][3]));
				assertThat(user.getLastName(), is(list[i][4]));
				assertThat(user.getAdministratorFlg(), is(list[i][5]));
				assertThat(user.getUpdateUserId(), is(list[i][6]));
				assertThat(user.getDelFlg(), is(list[i][8]));
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void getByIdTest() {
		UserDao userDao = new UserDao();
		try {
			User user = userDao.getById(1);
			Object[] list = {1, "nobunaga_oda@gmail.com","a61dcc4d3422bdbc14c1a69b030ee709713127dd0b8b51491ff7b886641154de", "信長", "織田", 0, 1, "2018-10-22 14:42:37", 0};
			assertThat(user.getUserId(), is(list[0]));
			assertThat(user.getEmail(), is(list[1]));
			assertThat(user.getFirstName(), is(list[3]));
			assertThat(user.getLastName(), is(list[4]));
			assertThat(user.getAdministratorFlg(), is(list[5]));
			assertThat(user.getUpdateUserId(), is(list[6]));
			assertThat(user.getDelFlg(), is(list[8]));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	void deleteTest() {
		UserDao userdao = new UserDao();
		try {
			userdao.deleteUser(14);
			assertThat(userdao.getById(14).getDelFlg(), is(1));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
