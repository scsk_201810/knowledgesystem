package jp.kronos.test;

import org.junit.jupiter.api.Test;

import jp.kronos.dao.UserDao;
import jp.kronos.dto.User;

class LoginTest {

	@Test
	void getUser() {
		String email = "nobunaga_oda@gmail.com";
		String pw = "nobunaga_oda";

		UserDao udao = new UserDao();
		User user = udao.getUserByIdAndPassword(email, pw);
		if(user == null) {
			System.out.println("null");
		} else {
			System.out.println(user.getFirstName());
		}
	}
}