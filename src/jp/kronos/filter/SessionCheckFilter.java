package jp.kronos.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.kronos.dto.User;

/**
 * Servlet Filter implementation class SessionCheckFilter
 */
@WebFilter(filterName="/SessionCheckFilter", urlPatterns= {"/DeleteChannelServlet", "/DeleteUserServlet", "/InsertKnowledgeServlet", "/KnowledgeServlet", "/ManagementUserServlet", "/UpdateKnowledgeServlet"})
public class SessionCheckFilter implements Filter {

    public SessionCheckFilter() {}
	public void destroy() {}
	/**
	 * @see  Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub1
		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = null;
		if(session.getAttribute("user") != null) {
			user = (User)session.getAttribute("user");
		}
		//		System.out.println(session);
		if (session == null || user == null) {
//		if (session.isNew()) {
//		if (HttpSession isNew()) {
			request.getRequestDispatcher("/timeout.jsp").forward(request, response);
		} else {
			// pass the request along the filter chain.
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub

	}
}
