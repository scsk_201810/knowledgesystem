package jp.kronos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.ChannelDao;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.Channel;
import jp.kronos.dto.Knowledge;

/**
 * Servlet implementation class Knowledge
 */
@WebServlet("/KnowledgeServlet")
public class KnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ChannelDao channelDao = new ChannelDao();
		try {
		List<Channel> list = channelDao.selectChannel();
		request.setAttribute("list", list);

		KnowledgeDao knowledgedao = new KnowledgeDao();
		Knowledge knowledge = knowledgedao.getKnowledge(Integer.parseInt(request.getParameter("knowledgeID")));
		request.setAttribute("knowledge", knowledge);
			} catch (Exception e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		request.getRequestDispatcher("/updateKnowledge.jsp").forward(request, response);
	}

}
