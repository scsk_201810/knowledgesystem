package jp.kronos.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.ChannelDao;

/**
 * Servlet implementation class DeleteChannelServlet
 */
@WebServlet("/DeleteChannelServlet")
public class DeleteChannelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteChannelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			ChannelDao channelDao= new ChannelDao();
			//パラメータ取得
			channelDao.deleteChannel(Integer.parseInt(request.getParameter("channelId")));
			request.setAttribute("deleteComplete", request.getParameter("channelName"));
			request.getRequestDispatcher("ThreadListServlet").forward(request, response);
		}catch(ClassNotFoundException|SQLException e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			request.setCharacterEncoding("UTF-8");
			ChannelDao channelDao= new ChannelDao();
			//パラメータ取得
			channelDao.deleteChannel(Integer.parseInt(request.getParameter("channelId")));
			request.setAttribute("deleteComplete", request.getParameter("channelName"));
			request.getRequestDispatcher("ThreadListServlet").forward(request, response);
		}catch(ClassNotFoundException|SQLException e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}
}
