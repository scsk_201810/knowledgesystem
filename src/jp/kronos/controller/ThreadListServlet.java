package jp.kronos.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.Channel;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/ThreadListServlet")
public class ThreadListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDao channelDao= new ChannelDao();
		try {
			//チャンネル一覧を取得
			List<Channel> channelList = channelDao.selectChannel();
			request.setAttribute("channelList", channelList);
			request.getRequestDispatcher("/threadList.jsp").forward(request, response);
		}catch(SQLException|ClassNotFoundException e) {
			//エラー時、システムエラー画面に遷移
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDao channelDao= new ChannelDao();
		try {
			//チャンネル一覧を取得
			List<Channel> channelList = channelDao.selectChannel();
			request.setAttribute("channelList", channelList);
			request.getRequestDispatcher("/threadList.jsp").forward(request, response);
		}catch(SQLException|ClassNotFoundException e) {
			//エラー時、システムエラー画面に遷移
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}
}
