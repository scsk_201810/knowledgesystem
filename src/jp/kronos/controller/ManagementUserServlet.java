package jp.kronos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.UserDao;
import jp.kronos.dto.User;

/**
 * Servlet implementation class ManagementUserServlet
 */
@WebServlet("/ManagementUserServlet")
public class ManagementUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagementUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDao userDao = new UserDao();
		List<User> userList;
		try {
			userList = userDao.selectUser();
			request.setAttribute("userList", userList);
			request.getRequestDispatcher("/managementUser.jsp").forward(request, response);
		} catch (Exception e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDao userDao = new UserDao();
		List<User> userList;
		try {
			userList = userDao.selectUser();
			request.setAttribute("userList", userList);
			request.getRequestDispatcher("/managementUser.jsp").forward(request, response);
		} catch (Exception e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
		}
}
