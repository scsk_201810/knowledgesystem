package jp.kronos.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.ChannelDao;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.Channel;
import jp.kronos.dto.Knowledge;

/**
 * Servlet implementation class KnowledgeListServlet
 */
@WebServlet("/KnowledgeListServlet")
public class KnowledgeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public KnowledgeListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//DAOの生成
			ChannelDao channelDao = new ChannelDao();
			KnowledgeDao knowledgeDao= new KnowledgeDao();

			//リクエストからチャンネルIDを取得
			int channelId = Integer.parseInt( request.getParameter("channelId"));
			//チャンネル情報の取得
			Channel channel = channelDao.getChannelById(channelId);
			request.setAttribute("channel", channel);

			//ナレッジ一覧の取得
			List<Knowledge> knowledgeList = knowledgeDao.getKnowledgeByChannelId(channelId);
			request.setAttribute("knowledgeList", knowledgeList);

			request.getRequestDispatcher("/listKnowledge.jsp").forward(request, response);
		}catch(SQLException|ClassNotFoundException exception) {
			//システムエラー画面遷移
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
