package jp.kronos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.UserDao;
import jp.kronos.dto.User;

/**
 * Servlet implementation class DeleteUser
 */
@WebServlet("/DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDao userDao = new UserDao();
		User user = null;
		int userId = Integer.parseInt(request.getParameter("userId"));
		try {
			userDao.deleteUser(userId);
		} catch (Exception e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
		try {
			user = userDao.getById(userId);
		} catch (Exception e) {
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
		request.setAttribute("message", user.getEmail() + "を削除しました。");
		request.getRequestDispatcher("/ManagementUserServlet").forward(request, response);
		}

}
