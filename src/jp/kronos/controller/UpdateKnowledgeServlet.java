package jp.kronos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.KnowledgeDao;

/**
 * Servlet implementation class UpdateKnowledge
 */
@WebServlet("/UpdateKnowledgeServlet")
public class UpdateKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

			int newKnowledgeID = Integer.parseInt(request.getParameter("knowledgeID")); //ナレッジ一覧からのナレッジIDの取得
			String newKnowledge = request.getParameter("knowledge");  //ナレッジの取得
			int newChannelID = Integer.parseInt(request.getParameter("channel")); //チャンネルIDの取得

			KnowledgeDao newknowledgedao = new KnowledgeDao();
			try {
			newknowledgedao.updateKnowledge(newKnowledgeID,newKnowledge,newChannelID);
			}catch(Exception e) {
				request.getRequestDispatcher("/error.jsp").forward(request, response);
			}
			request.setAttribute("channelId",newChannelID);
			request.getRequestDispatcher("KnowledgeListServlet?channelId=" + newChannelID).forward(request, response);
		}
	}
