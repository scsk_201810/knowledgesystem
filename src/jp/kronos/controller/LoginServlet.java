package jp.kronos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.dao.UserDao;
import jp.kronos.dto.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォームの値を取得する
		String email = request.getParameter("mail");
		String pw = request.getParameter("pw");

		// セッション情報の取得
		HttpSession session = request.getSession();


		if ("".equals(email) || "".equals(pw)) {
			// メールアドレスorパスワードが未入力

			// セッションのエラーメッセージに"メール～さい。"を格納。
			session.setAttribute("headerMsg", "メールアドレスとパスワードを入力してください。");

			// 遷移元のページ情報を取得。
			String currentUrl = request.getHeader("Referer");
			String [] currentPage = currentUrl.split("[/]");

			// JSP側に処理を返す。
//			response.sendRedirect(currentPage[currentPage.length-1]);
			request.getRequestDispatcher(currentPage[currentPage.length-1]).forward(request, response);
//			response.sendRedirect(new URI(request.getHeader("referer")).getPath());
		}

		// データベース照合のクラスを呼び出し。
		UserDao udao = new UserDao();
		// 入力されたユーザー情報を取得
		User user = udao.getUserByIdAndPassword(email, pw);

		// セッションに格納したヘッダのエラーメッセージ(headerMsg)を削除
		session.removeAttribute("headerMsg");

		if (user != null) {
		// 分岐：認証成功

			// セッションにユーザー情報を格納する
			session.setAttribute("user", user);
			// セッションのタイムアウト時間を設定
//			session.setMaxInactiveInterval(15);

			// 遷移元のページ情報を取得。
			String currentUrl = request.getHeader("Referer");
			String [] currentPage = currentUrl.split("[/]");

			// JSP側に処理を返す。
			response.sendRedirect(currentPage[currentPage.length-1]);

		} else {
			// セッションのエラーメッセージを更新。
			session.setAttribute("headerMsg", "メールアドレスとパスワードの組み合わせが異なります。");

			// 遷移元のページ情報を取得。
			String currentUrl = request.getHeader("Referer");
			String [] currentPage = currentUrl.split("[/]");

			// JSP側に処理を返す。
			request.getRequestDispatcher(currentPage[currentPage.length-1]).forward(request, response);
		}
	}
}