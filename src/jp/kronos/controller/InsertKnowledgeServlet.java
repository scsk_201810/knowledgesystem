package jp.kronos.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.Knowledge;

/**
 * Servlet implementation class InsertKnowledgeServlet
 */
@WebServlet("/InsertKnowledgeServlet")
public class InsertKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertKnowledgeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			//ナレッジDAO、ナレッジ生成
			KnowledgeDao knowledgeDao= new KnowledgeDao();
			Knowledge knowledge = new Knowledge();

			//文字数チェック
			String text = request.getParameter("knowledge");
			if(text==null||text.equals("")){
				request.setAttribute("errorMassage", "ナレッジが未入力です。");
				request.getRequestDispatcher("KnowledgeListServlet").forward(request, response);
			}else if(text.length()>500){
				request.setAttribute("errorMassage", "ナレッジは500文字以内で入力してください。");
				request.getRequestDispatcher("KnowledgeListServlet").forward(request, response);
			}else {
				//ナレッジのデータ入力
				knowledge.setKnowledge(text);
				knowledge.setChannelId(Integer.parseInt(request.getParameter("channelId")));
				knowledge.setUserId(Integer.parseInt(request.getParameter("userId")));

				//ナレッジ追加
				knowledgeDao.insertKnowledge(knowledge);

				request.getRequestDispatcher("KnowledgeListServlet").forward(request, response);
			}

		}catch(SQLException|ClassNotFoundException exception) {
			//システムエラー画面遷移
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

}
