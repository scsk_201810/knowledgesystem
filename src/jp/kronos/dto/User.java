package jp.kronos.dto;

import java.sql.Timestamp;

public class User {

	// ユーザID
	int userId;

	// メールアドレス
	String email;

	// パスワード
	String password;

	// 名
	String firstName;

	// 姓
	String lastName;

	// 管理者フラグ
	int administratorFlg;

	// 更新ユーザID
	int updateUserId;

	// 最終更新日時
	Timestamp updateAt;

	// 削除フラグ
	int delFlg;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAdministratorFlg() {
		return administratorFlg;
	}
	public void setAdministratorFlg(int administratorFlg) {
		this.administratorFlg = administratorFlg;
	}
	public int getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Timestamp getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}
	public int getDelFlg() {
		return delFlg;
	}
	public void setDelFlg(int delFlg) {
		this.delFlg = delFlg;
	}

}
