package jp.kronos.dto;

import java.sql.Timestamp;

public class Channel {

	// チャンネルID
	int channelId;

	// チャンネル名
	String channelName;

	// 概要
	String overview;

	// ユーザID
	int userId;

	// 最終更新日時
	Timestamp updateAt;

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Timestamp getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}


}
