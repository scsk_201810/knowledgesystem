<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ナレッジ管理システム</title>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/timeout.css">
</head>
<body>
<jsp:include page="header.jsp" />
<div class="timeout">
	<h1>セッションが無効であるか、タイムアウトしたため再ログインが必要です。</h1>
</div>
</body>
</html>