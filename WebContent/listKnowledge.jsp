<%@page import="java.text.SimpleDateFormat"%>
<%@page import="jp.kronos.dto.Channel"%>
<%@page import="jp.kronos.dto.User"%>
<%@page import="jp.kronos.dto.Knowledge"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ナレッジ管理システム</title>
<link rel="stylesheet" href="css/listKnowledge.css">
<link rel="stylesheet" href="css/header.css">
</head>
<body>
<%
Channel ch = (Channel)request.getAttribute("channel");
%>

<jsp:include page="header.jsp" />

<section>
	<h2><%= ch.getChannelName() %></h2>
	<form class="form_back" action="ThreadListServlet" method="get">
		<input class="btn_back" type="submit" value="スレッド一覧へ戻る" onclick="location.href='RedirectFilterServlet'">
	</form>
	<h5><%= ch.getOverview() %></h5>
	<hr><br>

	<article>
		<% if(request.getAttribute("errorMassage") != null){ %>
			<label><%= request.getAttribute("errorMassage") %></label>
		<% } %>
		<% User loginUser =(User) session.getAttribute("user");%>
		<%if(loginUser!=null){ %>
		<form action="InsertKnowledgeServlet" method="post" >
			<textarea rows="5" name="knowledge" placeholder="あなたのナレッジを共有しましょう！" onkeydown="wupBtn()" id="text"></textarea><br>
			<input type="hidden" name="channelId" value="<%= ch.getChannelId() %>">
			<input type="hidden" name="userId" value="<%= loginUser.getUserId() %>">
			<input class="btn_base btn_submit" type="submit" value="送信" id="btn" disabled >
		</form><%}%><br><br>

	<%List<Knowledge> knowledgeList = (List<Knowledge>)request.getAttribute("knowledgeList");%>
 <% for(Knowledge knowledge : knowledgeList){ %>
		<table>
			<tr>
				<td class="td_haed">
					<%if(loginUser==null){ %>
					<%=knowledge.getUserName() %>&emsp;<%=knowledge.getUpdateAt() %>
					<%}else if(knowledge.getUserId() == loginUser.getUserId()){ %>
					<a href="/KnowledgeSystem/KnowledgeServlet?knowledgeID=<%= knowledge.getKnowledgeId() %>"><%=knowledge.getUserName() %></a>&emsp;<%=knowledge.getUpdateAt() %>
					<input class="btn_base btn_delete" type="button" value="削除">
					<%} else{ %>
					<%=knowledge.getUserName() %>&emsp;<%=knowledge.getUpdateAt() %>
					<%} %>
				</td>
			</tr>
			<tr>
				<td class="td_content"><%=knowledge.getKnowledge() %></td>
			</tr>
		</table>
		<%}%>
	</article>
</section>
<script type="text/javascript">

<!--
function wupBtn(){
	if(document.getElementById("text").value == ""){
		document.getElementById("btn").disabled = true;
	}else{
		document.getElementById("btn").disabled = false;
	}
}
// -->

</script>
</body>
</html>