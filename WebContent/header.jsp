<%@page import="jp.kronos.dto.User"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<!-- ユーザ情報の取得 -->
<% User user = (User)session.getAttribute("user"); %>
<header>
<a class="link" href= RedirectFilterServlet>ナレッジ管理システム</a>
<div class="divMsg">
	<span class="errorMsg">
		<% if(session.getAttribute("headerMsg") != null){ %>
			<%= session.getAttribute("headerMsg") %>
		<% } %>

	</span>
</div><br>

<div class="hdiv">
	<form class="head_form" action="RedirectFilterServlet" method="get">
		<input type="text" name="search" placeholder="ナレッジ検索(Inactive)">
		<input type="submit" value="検索" >
	</form>&emsp;
	<!-- ユーザ情報がない場合  -->
	<% if(user == null) { %>
	<form class="head_form" action="LoginServlet" method="post">
		<input type="text" name="mail" placeholder="メールアドレス">
		<input type="password" name="pw" placeholder="パスワード">
		<input type="submit" value="ログイン">
	</form>
	<% 		}else if(user.getAdministratorFlg() == 1){%>
	<!-- 管理者フラグが有効の場合 -->
	<% session.setMaxInactiveInterval(15); %>
	<form class="head_form" action="LogoutServlet" method="post">
		<input class="logout" type="submit" value="ログアウト">
	</form>
	<form class="head_form" action="ManagementUserServlet" method="post">
		<input class="logout" type="submit" value="ユーザー管理">
	</form>
	<%		}else{ %>
	<!-- その他 -->
	<% session.setMaxInactiveInterval(15); %>
	<form class="head_form" action="LogoutServlet" method="post">
		<input class="logout" type="submit" value="ログアウト">
	</form>
	<% 		}	%>

</div>
</header>
