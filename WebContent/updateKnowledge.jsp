<%@page import="jp.kronos.dto.Channel"%>
<%@page import="jp.kronos.dto.Knowledge"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/updateKnowledge.css">
</head>
<body>
<jsp:include page="header.jsp" />
	<div class="update_message">
		<h2>ナレッジ情報更新</h2>
		<br>
		<br>
		スレッド
	</div>
	<form action="UpdateKnowledgeServlet" method="Post">
	<div class="update_select">
		<select  name="channel" style="width:600px;height:50px" >
		<% List<Channel> list = (List<Channel>)request.getAttribute("list"); %>
		<% Knowledge knowledge=(Knowledge)request.getAttribute("knowledge"); %>
		<%for(int i=0;i<list.size();i++){%>
			<%Channel ChannelList =list.get(i); %>
				<%if(ChannelList.getChannelId()==knowledge.getChannelId()){%>
		   		<option value="<%=ChannelList.getChannelId() %>" selected>
				<%}else{%>
		   		<option value="<%=ChannelList.getChannelId() %>">
				<%}%>
		   		<%= ChannelList.getChannelName() %>
		   		</option>
		<% }%>
		</select>
	</div>
		<br>
	<div class="update_knowledgemessage">
		ナレッジ
	</div>
	<div class="update_textbox">
		<textarea  name="knowledge"  style="width:600px;height:250px"><%=knowledge.getKnowledge()%>
</textarea>
	</div>
		<br>
		<br>
		<br>
	<div class="update_button">
		<input class="update_button2" type="submit" value="更新" style="width:80px;height:30px">
	</div>
	<input type="hidden" name="knowledgeID" value="<%= knowledge.getKnowledgeId() %>">
	</form>
</body>
</html>