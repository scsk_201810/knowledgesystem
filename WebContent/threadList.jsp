<%@page import="jp.kronos.dto.User"%>
<%@page import="java.util.List"%>
<%@page import="jp.kronos.dto.Channel" %>
<%@page import="jp.kronos.controller.ThreadListServlet" %>
<%@page import="jp.kronos.controller.DeleteChannelServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ナレッジ管理システム</title>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/threadList.css">
</head>
<body>
<jsp:include page="header.jsp" />
<% User loginUser =(User) session.getAttribute("user");%>
<div class="threadList">
	<h2>スレッド一覧</h2>
</div>


<%if(loginUser!=null){ %>
<div class="createNewThreadButton">
	<input class="createNewThread" type="button" value="新規作成">
</div>
<%	}else{
		loginUser=new User();
		loginUser.setUserId(0);
	}
%>
<br>
<%String deleteChannelName = (String)request.getAttribute("deleteComplete"); %>
<% if(deleteChannelName!=null){%>
<div class="deleteMessage">
	スレッド「<%=deleteChannelName %>」を削除しました。
</div>
<%} %>

<%List<Channel> channelList = (List<Channel>)request.getAttribute("channelList");%>
<table class="channel_table">
	<tbody>
	<tr><th>#</th><th>スレッド名</th><th>概要</th><%if(loginUser.getUserId()!=0){ %><th class="row-button"></th><%} %></tr>
	<% int i=1; %>
	<% for(Channel channel : channelList){ %>
		<tr>
			<td><%= i %></td>
			<td><a href="/KnowledgeSystem/KnowledgeListServlet?channelId=<%= channel.getChannelId() %>"><%= channel.getChannelName() %></a></td>
			<td><%= channel.getOverview() %></td>
			<%if(loginUser.getUserId()==channel.getUserId()){ %>
			<td>
			&nbsp;
				<input class="updateThread" type="button" value="編集" >
			&nbsp;
				<input class="deleteThread" type="button" value="削除" onClick="dialog('<%= channel.getChannelId()%>','<%= channel.getChannelName()%>')" >
			&nbsp;
			</td>
			<%}else if(loginUser.getUserId()!=0){%>
			<td>
			</td>
			<%} %>
			</tr>
	<%i++; %>
	<%} %>
	</tbody>
</table>


<form action="DeleteChannelServlet" method="post" name="deleteChannel"><input type="hidden" name="channelId" value="" id="channelId"><input type="hidden" name="channelName" value="" id="channelName"></form>

<script type="text/javascript">
<!--
function dialog(channelId, channelName){
	if(window.confirm("スレッド「" + channelName + "」を本当に削除しますか？")){
		document.getElementById("channelId").value = channelId;
		document.getElementById("channelName").value = channelName;
		document.deleteChannel.submit();
	}
}
// -->

</script>
</body>
</html>