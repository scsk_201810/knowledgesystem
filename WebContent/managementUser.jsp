<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jp.kronos.dto.User" %>
<%@page import="jp.kronos.controller.ManagementUserServlet" %>
<%@page import="jp.kronos.controller.DeleteUserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ナレッジ管理システム</title>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/managementUser.css">
</head>
<body>
<jsp:include page="header.jsp" />

<div class="managementuser">
	<h2 >ユーザー管理
	<input class="back_to_list" type="button" value="スレッド一覧へ戻る" onclick="location.href='RedirectFilterServlet'">
	</h2>
	<% if(request.getAttribute("message") != null){ %>
		<label><%= request.getAttribute("message") %></label>
	<% } %>
</div>
<%List<User> userList = (List<User>) request.getAttribute("userList");%>
<table class="user_table">
	<thead>
		<tr>
			<th>メールアドレス</th>
			<th>姓</th>
			<th>名</th>
			<th>管理権限</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><input type="text" name="mail" placeholder="例）sample@example.com" class="mailbox"></td>
			<td><input type="text" name="last_name" class="namebox"></td>
			<td><input type="text" name="first_name" class="namebox"></td>
			<td><input type="checkbox" name="administrator_flg" class="admin_flg"></td>
			<td><input type="submit" value="追加" class="add_button"></td>
		</tr>
		<% for(User user : userList){ %>
		<tr>
			<td><%= user.getEmail() %></td>
			<td><%= user.getLastName() %></td>
			<td><%= user.getFirstName() %></td>
			<% if(user.getAdministratorFlg() == 1){ %>
				<td><input type="checkbox" name="admonistraror_flg" checked="checked" disabled="disabled"></td>
			<%}else{ %>
				<td><input type="checkbox" name="admonistraror_flg" disabled="disabled"></td>
			<%} %>
				<td>
					<input type="button" value="編集" class="edit_button">
					<input type="button" value="削除" onClick="disp('<%= user.getEmail() %>','<%= user.getUserId() %>' )" class="delete_button">
				</td>
		</tr>
		<%} %>
	</tbody>
</table>
<form action="DeleteUserServlet" method="post" id="delete">
	<input type="hidden" name="userId" value="" id="userId">
	<input type="hidden" name="email" value="" id="email">
</form>

<script type="text/javascript">

<!--
function disp(email,id){
	if(window.confirm('ユーザ「' + email + '」を本当に削除しますか？')){
		document.getElementById("userId").value = id;
		document.getElementById("email").value = email;
		document.getElementById("delete").submit();
	}
}
// -->

</script>
</body>
</html>