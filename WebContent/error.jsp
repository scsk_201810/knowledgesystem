<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ナレッジ管理システム</title>
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/error.css">
</head>
<body>
<jsp:include page="header.jsp" />
<div class="error_message">
	<h1>システムエラーが発生しました！</h1>
	<h5>大変お手数ですが、開発者までお問い合わせください。</h5>
</div>
</body>
</html>